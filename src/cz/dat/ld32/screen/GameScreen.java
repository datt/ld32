package cz.dat.ld32.screen;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.World;
import cz.dat.ld32.level.WorldLoader;
import cz.dat.ld32.level.tile.*;

import java.util.Random;

public class GameScreen extends ScreenBase {

    private WorldLoader loader;
    private World w;
    private World loaded[] = new World[3];
    private int currentWorld = 0;

    public GameScreen(Game game) {
        super(game, "Game");

        Tile.initTiles(game.getApi().getTexture());
        this.loader = new WorldLoader((TheGame) this.game);
        this.loadWorld(this.currentWorld);
        //this.testMap();
    }

    public void loadWorld(int world) {
        try {
            this.setWorld(this.loader.loadWorld(world));
            this.loaded[this.currentWorld] = this.w;
        } catch (Exception ignored) {
        }
    }

    public void setWorld(World w) {
        if (this.w != null)
            this.game.getApi().getInput().removeEventListener(this.w);

        this.w = w;
        if (this.game.getScreen() == this)
            this.game.getApi().getInput().addEventListener(this.w);
    }

    public void testMap() {
        TileMap t = new TileMap(25, 25);

        Random rnd = new Random();

        for (int i = 0; i < 25; i++) {
            for (int y = 0; y < 25; y++) {
                t.setTile(i, y, 0, rnd.nextBoolean() ? Tile.concrete : (rnd.nextBoolean() ? Tile.grass : DirtTile.dirt));
            }

            t.setTile(i, 0, 1, WallTile.wall);
            t.setTile(i, 24, 1, WallTile.wall);
            t.setTile(0, i, 1, WallTile.wall);
            t.setTile(24, i, 1, WallTile.wall);
            t.setTile(4, i, 1, WallTile.wall);
            t.setTile(i, 8, 1, WallTile.wall);
        }

        for (int i = 1; i < 4; i++) {
            t.setTile(i, 12, 1, WallTile.wall);
        }

        t.setTile(12, 0, 1, DoorTile.door);
        t.setTile(13, 0, 1, DoorTile.door);
        t.setTile(0, 10, 1, DoorTile.door);
        t.setTile(0, 11, 1, DoorTile.door);
        t.setTile(24, 10, 1, DoorTile.door);
        t.setTile(24, 11, 1, DoorTile.door);

        t.checkBlocks();

        this.w = new World(t, (TheGame) game);
    }

    @Override
    public void tick() {
        this.w.tick();

        if (this.w.shouldAnotherLevel() != -1) {
            // sexy effect
            this.currentWorld = this.w.shouldAnotherLevel();
            this.w.setNextLevel(-1);

            if (this.loaded[this.currentWorld] == null)
                this.loadWorld(this.currentWorld);
            else
                this.setWorld(this.loaded[this.currentWorld]);
        }
    }

    @Override
    public void onClosing() {
        super.onClosing();
        this.game.getApi().getRenderer().translate(0, 0, false);
        this.game.getApi().getInput().setMouseCursorVisible(true);

        if (this.w != null)
            this.game.getApi().getInput().removeEventListener(this.w);
    }

    @Override
    public void onOpening() {
        super.onOpening();
        this.game.getApi().getInput().setMouseCursorVisible(false);

        if (this.w != null)
            this.game.getApi().getInput().addEventListener(this.w);
    }

    @Override
    public void onKeyDown(int key) {
        if (key == IInputManager.Keys.ESCAPE)
            this.game.openScreen(0);
    }

    @Override
    public void renderTick(float ptt) {
        float xt = -this.w.getCamera().getX(ptt) * w.getBlockSize() + this.game.getWindow().getCentre().x();
        float yt = -this.w.getCamera().getY(ptt) * w.getBlockSize() + this.game.getWindow().getCentre().y();
        this.game.getApi().getRenderer().translate(xt, yt, false);
        this.w.renderTick(ptt);
    }
}
