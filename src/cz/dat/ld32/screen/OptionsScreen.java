package cz.dat.ld32.screen;

import cz.dat.gaben.api.data.IDataProvider;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.*;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;
import cz.dat.ld32.TheGame;

import java.util.ArrayList;
import java.util.List;
import java.util.function.Consumer;
import java.util.function.Function;


public class OptionsScreen extends ScreenBase {
    private IFontRenderer f;
    private IFont font;
    private IRenderer r;
    private ITexture arrow;
    private ITexture arrowb;

    private List<OptionsItem<?>> options;

    private float startY;
    private int selected = 0;

    public OptionsScreen(Game game) {
        super(game, "Options");
        this.addOptions();

        this.f = game.getApi().getFontRenderer();
        this.font = game.getApi().getFont().getFont("basic");
        this.r = game.getApi().getRenderer();

        this.f.setFont(this.font);
        this.f.setSize(42);
        this.f.setAnchor(Anchor.TOP_CENTER);
        
        this.arrow = game.getApi().getTexture().getTexture(1);
        this.arrowb = game.getApi().getTexture().getTexture(2);

        this.startY = (this.game.getHeight() / 2) - ((this.f.getMaxHeight() * this.options.size()) / 2);

        
    }

    private void addOptions() {
        IDataProvider p = ((TheGame) this.game).getDataProvider();
        this.options = new ArrayList<>();

        OptionsItem<Vector2> it = new OptionsItem<>();
        it.name = "Resolution";
        it.id = 0;
        it.formatter = (v -> ((int) v.x()) + "x" + ((int) v.y()));
        it.onChange = (v -> {
            p.setData("width", (int) v.x(), Integer.class);
            p.setData("height", (int) v.y(), Integer.class);
        });

        it.values.add(new Vector2(640, 480));
        it.values.add(new Vector2(800, 600));
        it.values.add(new Vector2(1024, 768));
        it.values.add(new Vector2(1280, 720));
        it.values.add(new Vector2(1280, 1024));
        it.values.add(new Vector2(1440, 900));
        it.values.add(new Vector2(1600, 900));
        it.values.add(new Vector2(1920, 1080));
        it.values.add(new Vector2(2560, 1440));
        it.values.add(new Vector2(3840, 2160));

        it.setCurrent(new Vector2(p.getData("width", Integer.class).floatValue(), p.getData("height", Integer.class).floatValue()));


        OptionsItem<Integer> snd = new OptionsItem<>();
        snd.name = "Sound level";
        snd.id = 1;
        snd.formatter = (i -> i.toString() + "%");
        snd.onChange = (v -> {
            p.setData("sound", v, Integer.class);
            this.game.getApi().getSound().setVolume(v != 0, v / 100f);
        });

        for (int i = 0; i <= 10; i++)
            snd.values.add((i * 10));

        snd.setCurrent(p.getData("sound", Integer.class));

        OptionsItem<String> nsa = new OptionsItem<>();
        nsa.name = "NSA level";
        nsa.id = 2;
        nsa.values.add("Audio");
        nsa.values.add("Video");
        nsa.values.add("All media");
        nsa.values.add("Keylogger");
        nsa.values.add("Everything");
        nsa.switchCurrent(4);

        OptionsItem<Boolean> blood = new OptionsItem<>();
        blood.name = "Bloody mode";
        blood.id = 3;
        blood.formatter = (b -> b ? "On" : "Off");
        blood.onChange = (b -> p.setData("blood", b, Boolean.class));
        blood.setCurrent(p.getData("blood", Boolean.class));

        blood.values.add(true);
        blood.values.add(false);

        this.options.add(it);
        this.options.add(snd);
        this.options.add(nsa);
        this.options.add(blood);
    }

    @Override
    public void onKeyDown(int key) {
        if (key == IInputManager.Keys.ESCAPE) {
            this.game.openScreen(0);
            this.game.getApi().getSound().playSound(1);
        }

        if (key == IInputManager.Keys.LEFT) {
            this.options.get(this.selected).switchCurrent(-1);
            this.game.getApi().getSound().playSound(0, 0.8f);
        }

        if (key == IInputManager.Keys.RIGHT) {
            this.options.get(this.selected).switchCurrent(1);
            this.game.getApi().getSound().playSound(0, 0.8f);
        }

        if (key == IInputManager.Keys.DOWN) {
            this.selected += 1;
            if (this.selected == this.options.size())
                this.selected = 0;

            this.game.getApi().getSound().playSound(0);
        }

        if (key == IInputManager.Keys.UP) {
            this.selected -= 1;
            if (this.selected < 0)
                this.selected = this.options.size() - 1;

            this.game.getApi().getSound().playSound(0);
        }
    }

    @Override
    public void tick() {

    }

    @Override
    public void renderTick(float ptt) {
        ((TheGame) this.game).drawClouds();

    	this.f.setFont(this.font);
        this.f.setSize(42);
        this.f.setAnchor(Anchor.TOP_CENTER);

        this.game.getApi().getRenderer().color(Color.WHITE);

        float bob = (float) (Math.sin(System.nanoTime()/200000000D)*10);
        
        float center = this.game.getWindow().getCentre().x();
        float offset = 240;
        
        for (int i = 0; i < this.options.size(); i++) {
            if (i == this.selected)
                this.game.getApi().getRenderer().color(TheGame.SEXY_BLUE);
            else
                this.game.getApi().getRenderer().color(Color.WHITE);

            this.f.drawString(this.options.get(i).name + ": " + this.options.get(i).getCurrent(),
                    this.game.getWindow().getCentre().x(), this.startY + i * this.f.getMaxHeight());
            
            this.r.drawTextureScaled(this.arrow, center + offset + bob, this.startY + i * this.f.getMaxHeight(), 0.7f);
            this.r.drawTextureScaled(this.arrowb, center - offset - bob - this.arrowb.getSize().x()*0.7f, this.startY + i * this.f.getMaxHeight(), 0.7f);
        }

    }

    private class OptionsItem<T> {
        public int id;
        public String name;
        public List<T> values;
        public T currentValue;
        public Function<T, String> formatter;
        public Consumer<T> onChange;
        int selected = 0;

        public OptionsItem() {
            this.values = new ArrayList<>();
            this.formatter = (Object::toString);
        }

        public String getCurrent() {
            return this.formatter.apply(this.currentValue);
        }

        public void setCurrent(T value) {
            this.currentValue = value;
            if (this.values.contains(currentValue))
                this.selected = this.values.indexOf(currentValue);
        }

        public String get(int i) {
            return this.formatter.apply(this.values.get(i));
        }

        public void switchCurrent(int add) {
            if (!this.values.contains(currentValue)) {
                this.selected = 0;
            }

            this.selected += add;

            if (this.selected == values.size())
                this.selected = 0;

            if (this.selected < 0)
                this.selected = values.size() - 1;

            this.currentValue = this.values.get(selected);

            if (this.onChange != null)
                this.onChange.accept(this.currentValue);
        }

        @Override
        public int hashCode() {
            return this.id;
        }
    }
}
