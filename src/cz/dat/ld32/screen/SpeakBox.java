package cz.dat.ld32.screen;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.interfaces.*;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Vector2;

import java.util.ArrayList;
import java.util.List;

public class SpeakBox implements ITickListener {

    private List<String[]> speechBubbles = new ArrayList<>();
    private int atBubble = 0;
    private float height;
    private Game game;
    private IApiContext context;
    private IFont font;
    private Color greyAlpha = new Color(Color.DARK_GREY.R(), Color.DARK_GREY.G(),
            Color.DARK_GREY.B(), 0.5f);
    private boolean visible = true;
    private boolean end = true;
    private ITexture enterTexture;
    private Vector2 enterPos;

    public SpeakBox(Game game) {
        this.game = game;
        this.context = game.getApi();
        this.font = context.getFont().getFont("basic");
        this.speechBubbles.add(new String[0]);
        this.enterTexture = game.getApi().getTexture().getTexture(5);
        this.enterPos = new Vector2(game.getWidth() - this.enterTexture.getSize().x() - 10,
                game.getHeight() - this.enterTexture.getSize().y() - 10);
    }

    public void addBubble(String[] lines) {
        this.speechBubbles.add(lines);
    }

    public void setBubble(int b) {
        this.context.getFontRenderer().setFont(this.font);
        this.context.getFontRenderer().setSize(30);
        this.context.getFontRenderer().setAnchor(Anchor.TOP_CENTER);

        this.atBubble = b;
        this.height = (context.getFontRenderer().getMaxHeight() + 3) * this.speechBubbles.get(b).length + 42;
        this.enterPos.setY(this.game.getHeight() - this.height / 2 - this.enterTexture.getSize().y() / 2);
    }

    public boolean next() {
        if (this.atBubble != (this.speechBubbles.size() - 1)) {
            this.setBubble(this.atBubble + 1);
            return true;
        } else {
            this.end = false;
            return false;
        }
    }

    public boolean isOnEnd() {
        return this.end;
    }

    public void setVisible(boolean visible) {
        this.visible = visible;
    }

    @Override
    public void tick() {
    }

    @Override
    public void renderTick(float ptt) {
        if (this.visible) {
            float tX, tY;
            tX = context.getRenderer().getTranslation().x();
            tY = context.getRenderer().getTranslation().y();

            context.getRenderer().translate(0, 0, false);

            float y = this.game.getHeight() - this.height;
            context.getRenderer().enableTexture(false);
            context.getRenderer().color(Color.DARK_GREY);
            context.getRenderer().shapeMode(IRenderer.ShapeMode.FILLED);
            context.getRenderer().drawRect(0, y, this.game.getWidth(), y + 2);
            y += 3;
            context.getRenderer().color(this.greyAlpha);
            context.getRenderer().drawRect(0, y, this.game.getWidth(), this.game.getHeight());
            y += 20;
            this.context.getFontRenderer().setFont(this.font);
            this.context.getFontRenderer().setSize(30);
            this.context.getFontRenderer().setAnchor(Anchor.TOP_CENTER);
            String[] lines = this.speechBubbles.get(this.atBubble);
            context.getRenderer().color(Color.BLACK);
            for (String l : lines) {
                context.getFontRenderer().drawString(l, this.game.getWindow().getCentre().x(), y);
                y += context.getFontRenderer().getMaxHeight() + 3;
            }

            context.getRenderer().color(Color.WHITE);
            context.getRenderer().drawTexture(this.enterTexture, this.enterPos);
            context.getRenderer().translate(tX, tY, false);
        }
    }
}
