package cz.dat.ld32.screen;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Rectangle;
import cz.dat.gaben.util.Vector2;
import cz.dat.ld32.TheGame;

import java.awt.*;
import java.io.IOException;
import java.net.URISyntaxException;
import java.net.URL;

public class AboutScreen extends ScreenBase {

    private IFontRenderer f;
    private IFont font;
    private ITexture flag;

    private float startY;
    private Rectangle linkRect;

    private String[] lines = new String[]
            {
                    "The Secret of MD",
                    "Created for Ludum Dare 32 on the 18th of April 2015",
                    "By LeOndryasO and dax105",
                    "Using GabeNgine Framework",
                    "Made in the Czech Republic",
                    "https://bitbucket.org/datt/ld32"
            };


    public AboutScreen(Game game) {
        super(game, "About");

        this.f = game.getApi().getFontRenderer();
        this.font = game.getApi().getFont().getFont("basic");
        this.flag = game.getApi().getTexture().getTexture(1);

        this.f.setFont(this.font);
        this.f.setSize(42);
        this.f.setAnchor(Anchor.TOP_CENTER);

        this.startY = this.game.getWindow().getCentre().y() - ((this.f.getMaxHeight() * lines.length) / 2);
        this.linkRect = new Rectangle(this.game.getWindow().getCentre().x() - this.f.getStringWidth(this.lines[this.lines.length - 1]) / 2,
                this.startY + (this.lines.length - 1) * this.f.getMaxHeight(), this.f.getStringWidth(this.lines[4]), this.f.getMaxHeight());
    }

    @Override
    public void onKeyDown(int key) {
        if (key == IInputManager.Keys.ESCAPE) {
            this.game.openScreen(0);
            this.game.getApi().getSound().playSound(1);
        }
    }

    @Override
    public void onMouseButtonDown(int button) {
        Vector2 pos = this.game.getApi().getInput().getMousePosition();

        if (button == IInputManager.Keys.MOUSE_LEFT && this.linkRect.contains(pos))
            try {
                Desktop.getDesktop().browse(new URL("https://bitbucket.org/datt/ld32").toURI());
            } catch (IOException | URISyntaxException ignored) {
            }
    }

    @Override
    public void tick() {

    }

    @Override
    public void renderTick(float ptt) {
        ((TheGame) this.game).drawClouds();

        this.game.getApi().getRenderer().color(Color.WHITE);

        this.f.setFont(this.font);
        this.f.setSize(42);
        this.f.setAnchor(Anchor.TOP_CENTER);

        for (int i = 0; i < this.lines.length; i++) {
            if (i == this.lines.length - 1)
                this.game.getApi().getRenderer().color(TheGame.SEXY_BLUE);

            this.f.drawString(this.lines[i], this.game.getWindow().getCentre().x(),
                    this.startY + i * this.f.getMaxHeight());
        }
    }
}
