package cz.dat.ld32.screen;

import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.ScreenBase;
import cz.dat.gaben.api.interfaces.IFont;
import cz.dat.gaben.api.interfaces.IFontRenderer;
import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.Anchor;
import cz.dat.gaben.util.Color;
import cz.dat.ld32.TheGame;

public class MenuScreen extends ScreenBase {
    private IFontRenderer f;
    private IFont font;
    private ITexture logo;

    private String[] lines = new String[] { "Play game", "Options", "About", "Exit" };
    private Integer[] defScreens = new Integer[] { 1, 2, 3, null };

    private int selectedOption = 0;

    public MenuScreen(Game game) {
        super(game, "Menu");

        this.f = game.getApi().getFontRenderer();
        this.font = game.getApi().getFont().getFont("basic");
        this.logo = game.getApi().getTexture().getTexture(0);
    }

    @Override
    public void onKeyDown(int key) {
        if(key == IInputManager.Keys.DOWN) {
            this.selectedOption++;

            if(this.selectedOption == this.lines.length)
                this.selectedOption = 0;

            this.game.getApi().getSound().playSound(0);
        }

        if(key == IInputManager.Keys.UP) {
            this.selectedOption--;

            if(this.selectedOption < 0)
                this.selectedOption = this.lines.length - 1;

            this.game.getApi().getSound().playSound(0);
        }

        if(key == IInputManager.Keys.ENTER){
            if(this.defScreens[this.selectedOption] != null) {
                this.game.openScreen(this.defScreens[this.selectedOption]);
            } else {
                if(this.selectedOption == 3) {
                    try {
                        this.game.getApi().getSound().playSound(1);
                        Thread.sleep(1500);
                    } catch (InterruptedException e) {
                        e.printStackTrace();
                    }

                    this.game.getWindow().shutdown(0);
                    return;
                }
            }

            this.game.getApi().getSound().playSound(1);
        }
    }

    @Override
    public void tick() {

    }

    @Override
    public void renderTick(float ptt) {
        ((TheGame) this.game).drawClouds();

        this.game.getApi().getRenderer().color(Color.WHITE);

        this.f.setFont(this.font);
        this.f.setSize(48);
        this.f.setAnchor(Anchor.TOP_CENTER);

        int y = 0;
        int i = 0;

        this.game.getApi().getRenderer().drawTexture(this.logo, this.game.getWindow().getCentre().x(), 10, Anchor.TOP_CENTER);
        y += this.logo.getSize().y() + 30;

        for(String s : this.lines) {
            if(i == this.selectedOption)
                this.game.getApi().getRenderer().color(TheGame.SEXY_BLUE);
            else
                this.game.getApi().getRenderer().color(Color.WHITE);

            this.f.drawString(s, this.game.getWindow().getCentre().x(), y);

            i++;
            y += this.f.getStringHeight(s) + 5;
        }
        
        
    }
}
