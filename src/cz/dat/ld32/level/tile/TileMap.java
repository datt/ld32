package cz.dat.ld32.level.tile;

public class TileMap {
    private int width, height;

    private Tile[][] layer0;
    private Tile[][] layer1;
    private Object[][] blockState0;
    private Object[][] blockState1;

    public TileMap(int tilesX, int tilesY) {
        this.layer0 = new Tile[tilesX][tilesY];
        this.layer1 = new Tile[tilesX][tilesY];
        this.blockState0 = new Object[tilesX][tilesY];
        this.blockState1 = new Object[tilesX][tilesY];

        for (int x = 0; x < tilesX; x++) {
            for (int y = 0; y < tilesY; y++) {
                this.layer0[x][y] = Tile.none;
                this.layer1[x][y] = Tile.none;
            }
        }

        this.width = tilesX;
        this.height = tilesY;
    }

    public Object getState(int x, int y, int layer) {
        if (x >= 0 && x < this.width && y >= 0 && y < this.height && (layer == 0 || layer == 1)) {
            if (layer == 0)
                return this.blockState0[x][y];
            return this.blockState1[x][y];
        }

        return null;
    }

    public void setState(int x, int y, int layer, Object state) {
        if (x >= 0 && x < this.width && y >= 0 && y < this.height && (layer == 0 || layer == 1)) {
            if (layer == 0)
                this.blockState0[x][y] = state;
            else
                this.blockState1[x][y] = state;
        }
    }

    public void setTile(int x, int y, int layer, Tile tile) {
        if (x >= 0 && x < this.width && y >= 0 && y < this.height && (layer == 0 || layer == 1)) {
            if (layer == 0)
                this.layer0[x][y] = tile;
            else
                this.layer1[x][y] = tile;
        }
    }

    public Tile getTileAt(int x, int y, int layer) {
        if (x >= 0 && x < this.width && y >= 0 && y < this.height && (layer == 0 || layer == 1)) {
            if (layer == 0)
                return this.layer0[x][y];
            return this.layer1[x][y];
        }

        return Tile.none;
    }

    public Tile[] getTilesAt(int x, int y) {
        if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
            return new Tile[]{this.layer0[x][y], this.layer1[x][y]};
        }

        return null;
    }

    public boolean canStepOn(int x, int y) {
        if (x >= 0 && x < this.width && y >= 0 && y < this.height) {
            return this.layer0[x][y].canStepOn(x, y, 0, this) && this.layer1[x][y].canStepOn(x, y, 1, this);
        }

        return false;
    }

    public void checkBlock(int x, int y, int layer) {
        if (x >= 0 && x < this.width && y >= 0 && y < this.height && (layer == 0 || layer == 1)) {
            if (layer == 0) {
                Tile t = this.layer0[x][y].shouldBeDifferent(x, y, 0, this);
                if (t != null)
                    this.layer0[x][y] = t;
            } else {
                Tile t = this.layer1[x][y].shouldBeDifferent(x, y, 1, this);
                if (t != null)
                    this.layer1[x][y] = t;
            }
        }
    }

    public void checkBlocksAround(int x, int y, int layer) {
        for (int xc = (x - 1); xc < (x + 1); xc++) {
            for (int yc = (y - 1); yc < (y + 1); yc++) {
                this.checkBlock(xc, yc, layer);
            }
        }
    }

    public void checkBlocks() {
        for (int x = 0; x < this.width; x++) {
            for (int y = 0; y < this.height; y++) {
                this.checkBlock(x, y, 0);
                this.checkBlock(x, y, 1);
            }
        }
    }

    public int getWidth() {
        return this.width;
    }

    public int getHeight() {
        return this.height;
    }
}
