package cz.dat.ld32.level.tile;

import cz.dat.gaben.api.interfaces.ITexture;

public class DoorTile extends Tile {

    public static Tile door, hL, hR, vU, vD, oU, oD, oL, oR;

    public DoorTile(ITexture texture) {
        super(texture);
    }

    @Override
    public boolean is(Tile other) {
        return (other instanceof DoorTile) || (other instanceof WallTile);
    }

    @Override
    public Tile shouldBeDifferent(int x, int y, int l, TileMap map) {
        int soulmateX = x, soulmateY = y;
        Tile ret = null;

        if (map.getTileAt(x, y - 1, l).is(WallTile.wall) && map.getTileAt(x, y + 1, l).is(DoorTile.door)) {
            soulmateY++;
            ret = this.canStepOn(x, y, l, map) ? DoorTile.oU : DoorTile.vU;
        } else if (map.getTileAt(x, y - 1, l).is(DoorTile.door) && map.getTileAt(x, y + 1, l).is(WallTile.wall)) {
            soulmateY--;
            ret = this.canStepOn(x, y, l, map) ? DoorTile.oD : DoorTile.vD;
        } else if (map.getTileAt(x - 1, y, l).is(WallTile.wall) && map.getTileAt(x + 1, y, l).is(DoorTile.door)) {
            soulmateX++;
            ret = this.canStepOn(x, y, l, map) ? DoorTile.oL : DoorTile.hL;
        } else if (map.getTileAt(x - 1, y, l).is(DoorTile.door) && map.getTileAt(x + 1, y, l).is(WallTile.wall)) {
            soulmateX--;
            ret = this.canStepOn(x, y, l, map) ? DoorTile.oR : DoorTile.hR;
        }

        int[] o = (int[]) map.getState(x, y, l);
        if (o == null) {
            o = new int[3];
            o[2] = 0;
        }

        o[0] = soulmateX;
        o[1] = soulmateY;

        map.setState(x, y, l, o);

        return ret;
    }

    @Override
    public boolean canStepOn(int x, int y, int l, TileMap map) {
        Object o = map.getState(x, y, l);
        if (o != null)
            return ((int[]) o)[2] == 1;

        return false;
    }
}
