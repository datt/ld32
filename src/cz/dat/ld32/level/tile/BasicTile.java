package cz.dat.ld32.level.tile;

import cz.dat.gaben.api.interfaces.ITexture;

public class BasicTile extends Tile {
    private boolean canStep = true;

    public BasicTile(ITexture texture) {
        super(texture);
    }

    public BasicTile setCanStepOn(boolean state) {
        this.canStep = state;
        return this;
    }

    @Override
    public Tile shouldBeDifferent(int x, int y, int l, TileMap map) {
        return null;
    }

    @Override
    public boolean canStepOn(int x, int y, int l, TileMap map) {
        return this.canStep;
    }
}
