package cz.dat.ld32.level.tile;

import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.api.interfaces.ITextureManager;

import java.util.HashMap;
import java.util.Map;

public abstract class Tile {
    public static Map<Integer, Tile> tileColors = new HashMap<>();
    public static Tile none, concrete, grass, tree;
    private ITexture texture;
    private int saveColor = -1;

    public Tile(ITexture texture) {
        this.texture = texture;
    }

    public static void initTiles(ITextureManager tm) {
        Tile.concrete = new BasicTile(tm.getTexture(0, 0)).setSaveColor(0xFFaaaaaa);
        Tile.grass = new BasicTile(tm.getTexture(0, 1)).setSaveColor(0xFF00ff00);
        Tile.tree = new BasicTile(tm.getTexture(0, 12)).setCanStepOn(false).setSaveColor(0xFF004500);
        Tile.none = new NothingTile();

        WallTile.wall = new WallTile(tm.getTexture(1, 0)).setSaveColor(0xFFda8000);
        WallTile.vertical = new WallTile(tm.getTexture(1, 1));
        WallTile.horizontal = new WallTile(tm.getTexture(1, 2));
        WallTile.cornerTL = new WallTile(tm.getTexture(1, 6));
        WallTile.cornerTR = new WallTile(tm.getTexture(1, 5));
        WallTile.cornerBL = new WallTile(tm.getTexture(1, 4));
        WallTile.cornerBR = new WallTile(tm.getTexture(1, 3));
        WallTile.tU = new WallTile(tm.getTexture(1, 7));
        WallTile.tD = new WallTile(tm.getTexture(1, 8));
        WallTile.tR = new WallTile(tm.getTexture(1, 9));
        WallTile.tL = new WallTile(tm.getTexture(1, 10));
        WallTile.cross = new WallTile(tm.getTexture(1, 11));

        DoorTile.door = new DoorTile(tm.getTexture(2, 0)).setSaveColor(0xFFad8000);
        DoorTile.hL = new DoorTile(tm.getTexture(2, 1));
        DoorTile.hR = new DoorTile(tm.getTexture(2, 2));
        DoorTile.vD = new DoorTile(tm.getTexture(2, 3));
        DoorTile.vU = new DoorTile(tm.getTexture(2, 4));
        DoorTile.oL = new DoorTile(tm.getTexture(2, 5));
        DoorTile.oR = new DoorTile(tm.getTexture(2, 6));
        DoorTile.oD = new DoorTile(tm.getTexture(2, 7));
        DoorTile.oU = new DoorTile(tm.getTexture(2, 8));

        DirtTile.dirt = new DirtTile(tm.getTexture(0, 2)).setSaveColor(0xFF603000);
        DirtTile.acd = new DirtTile(tm.getTexture(0, 7));
        DirtTile.ab = new DirtTile(tm.getTexture(0, 3));
        DirtTile.cd = new DirtTile(tm.getTexture(0, 4));
        DirtTile.b = new DirtTile(tm.getTexture(0, 5));
        DirtTile.d = new DirtTile(tm.getTexture(0, 6));
        DirtTile.ac = new DirtTile(tm.getTexture(0, 8));
        DirtTile.bd = new DirtTile(tm.getTexture(0, 9));
        DirtTile.a = new DirtTile(tm.getTexture(0, 10));
        DirtTile.c = new DirtTile(tm.getTexture(0, 11));
    }

    public static Tile getTile(int color) {
        if (color == 0xFFFFFFFF)
            return Tile.none;

        if (Tile.tileColors.containsKey(color)) {
            return Tile.tileColors.get(color);
        }

        return null;
    }

    public int getSaveColor() {
        return this.saveColor;
    }

    public Tile setSaveColor(int color) {
        this.saveColor = color;
        Tile.tileColors.put(color, this);
        return this;
    }

    public ITexture getTexture() {
        return this.texture;
    }

    public boolean is(Tile[] oneOfOther) {
        for (Tile t : oneOfOther) {
            if (this.is(t))
                return true;
        }

        return false;
    }

    public boolean is(Tile other) {
        return other == this;
    }

    public abstract Tile shouldBeDifferent(int x, int y, int l, TileMap map);

    public abstract boolean canStepOn(int x, int y, int l, TileMap map);
}

