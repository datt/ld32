package cz.dat.ld32.level.tile;

public class NothingTile extends Tile {
    public NothingTile() {
        super(null);
    }

    @Override
    public Tile shouldBeDifferent(int x, int y, int l, TileMap map) {
        return null;
    }

    @Override
    public boolean canStepOn(int x, int y, int l, TileMap map) {
        return true;
    }
}
