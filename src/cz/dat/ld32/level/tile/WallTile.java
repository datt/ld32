package cz.dat.ld32.level.tile;

import cz.dat.gaben.api.interfaces.ITexture;

public class WallTile extends Tile {

    public static Tile wall, vertical, horizontal, cornerTL, cornerTR, cornerBL, cornerBR, tU, tD, tR, tL, cross;

    public WallTile(ITexture texture) {
        super(texture);
    }

    @Override
    public boolean is(Tile other) {
        return (other instanceof WallTile);
    }

    @Override
    public Tile shouldBeDifferent(int x, int y, int l, TileMap map) {
        if (map.getTileAt(x - 1, y, l).is(WallTile.wall) && map.getTileAt(x, y - 1, l).is(WallTile.wall)
                && map.getTileAt(x + 1, y, l).is(WallTile.wall) && map.getTileAt(x, y + 1, l).is(WallTile.wall)) {
            return WallTile.cross; // +
        } else if (map.getTileAt(x - 1, y, l).is(WallTile.wall) && map.getTileAt(x, y + 1, l).is(WallTile.wall)
                && map.getTileAt(x + 1, y, l).is(WallTile.wall)) {
            return WallTile.tD; // ¨|¨
        } else if (map.getTileAt(x - 1, y, l).is(WallTile.wall) && map.getTileAt(x + 1, y, l).is(WallTile.wall)
                && map.getTileAt(x, y - 1, l).is(WallTile.wall)) {
            return WallTile.tU; // _|_
        } else if (map.getTileAt(x + 1, y, l).is(WallTile.wall) && map.getTileAt(x, y - 1, l).is(WallTile.wall)
                && map.getTileAt(x, y + 1, l).is(WallTile.wall)) {
            return WallTile.tR; // |-
        } else if (map.getTileAt(x, y - 1, l).is(WallTile.wall) && map.getTileAt(x - 1, y, l).is(WallTile.wall)
                && map.getTileAt(x, y + 1, l).is(WallTile.wall)) {
            return WallTile.tL; // -|
        } else if (map.getTileAt(x - 1, y, l).is(WallTile.wall) && map.getTileAt(x, y - 1, l).is(WallTile.wall)) {
            return WallTile.cornerBR; // _|
        } else if (map.getTileAt(x, y - 1, l).is(WallTile.wall) && map.getTileAt(x + 1, y, l).is(WallTile.wall)) {
            return WallTile.cornerBL; // |_
        } else if (map.getTileAt(x - 1, y, l).is(WallTile.wall) && map.getTileAt(x, y + 1, l).is(WallTile.wall)) {
            return WallTile.cornerTR; // ¨|
        } else if (map.getTileAt(x + 1, y, l).is(WallTile.wall) && map.getTileAt(x, y + 1, l).is(WallTile.wall)) {
            return WallTile.cornerTL; // |¨
        } else if (map.getTileAt(x, y - 1, l).is(WallTile.wall) && map.getTileAt(x, y + 1, l).is(WallTile.wall)) {
            return WallTile.vertical; // |
        } else if (map.getTileAt(x - 1, y, l).is(WallTile.wall) && map.getTileAt(x + 1, y, l).is(WallTile.wall)) {
            return WallTile.horizontal; // -
        }

        return null;
    }

    @Override
    public boolean canStepOn(int x, int y, int l, TileMap map) {
        return false;
    }
}
