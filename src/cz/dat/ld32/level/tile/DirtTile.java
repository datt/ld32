package cz.dat.ld32.level.tile;

import cz.dat.gaben.api.interfaces.ITexture;

public class DirtTile extends Tile {
    public static Tile dirt, acd, ab, cd, a, b, c, d, ac, bd;

    public DirtTile(ITexture texture) {
        super(texture);
    }

    @Override
    public boolean is(Tile other) {
        return other instanceof DirtTile;
    }

    @Override
    public Tile shouldBeDifferent(int x, int y, int l, TileMap map) {
        if (map.getTileAt(x - 1, y, l).is(Tile.grass) && map.getTileAt(x, y + 1, l).is(Tile.grass)
                && map.getTileAt(x + 1, y, l).is(Tile.grass)) {
            return DirtTile.acd;
        } else if (map.getTileAt(x + 1, y, l).is(Tile.grass) && map.getTileAt(x, y + 1, l).is(Tile.grass)) {
            return DirtTile.cd;
        } else if (map.getTileAt(x - 1, y, l).is(Tile.grass) && map.getTileAt(x, y - 1, l).is(Tile.grass)) {
            return DirtTile.ab;
        } else if (map.getTileAt(x + 1, y, l).is(Tile.grass) && map.getTileAt(x - 1, y, l).is(Tile.grass)) {
            return DirtTile.ac;
        } else if (map.getTileAt(x, y - 1, l).is(Tile.grass) && map.getTileAt(x, y + 1, l).is(Tile.grass)) {
            return DirtTile.bd;
        } else if (map.getTileAt(x, y - 1, l).is(Tile.grass)) {
            return DirtTile.b;
        } else if (map.getTileAt(x, y + 1, l).is(Tile.grass)) {
            return DirtTile.d;
        } else if (map.getTileAt(x - 1, y, l).is(Tile.grass)) {
            return DirtTile.a;
        } else if (map.getTileAt(x + 1, y, l).is(Tile.grass)) {
            return DirtTile.c;
        }

        return null;
    }

    @Override
    public boolean canStepOn(int x, int y, int l, TileMap map) {
        return true;
    }
}
