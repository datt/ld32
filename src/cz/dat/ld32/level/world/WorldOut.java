package cz.dat.ld32.level.world;

import cz.dat.gaben.util.Rectangle;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.World;
import cz.dat.ld32.level.tile.TileMap;

public class WorldOut extends World {
    private Rectangle startRect = new Rectangle(43, 39, 2, 1);
    private Rectangle endRect = new Rectangle(2, 33, 1, 2);

    public WorldOut(TileMap map, TheGame game) {
        super(map, game);
    }

    @Override
    public void tick() {
        super.tick();

        if (this.getPlayer().getBB().intersects(this.startRect)) {
            this.getPlayer().getBB().setPosition(this.getPlayer().getBB().x1(),
                    this.getPlayer().getBB().y1() - 1.5f);
            this.shouldEnd = 0;
        }

        if (this.getPlayer().getBB().intersects(this.endRect))
            this.shouldEnd = 2;
    }
}
