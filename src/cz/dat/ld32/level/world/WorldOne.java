package cz.dat.ld32.level.world;

import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.util.Rectangle;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.World;
import cz.dat.ld32.level.entity.Player;
import cz.dat.ld32.level.tile.TileMap;
import cz.dat.ld32.screen.SpeakBox;

import java.io.IOException;

public class WorldOne extends World {
    private Rectangle endRect = new Rectangle(4, 1, 2, 1);
    private SpeakBox box;

    public WorldOne(TileMap map, TheGame game) throws IOException {
        super(map, game);
        this.box = new SpeakBox(game);
        this.box.addBubble(new String[]{"Welcome to PicoPseudo Laboratories testing room No. 69"});
        this.box.addBubble(new String[]{"According to this contract",
                "you've been equipped with the MD weapon"});
        this.box.addBubble(new String[]{"When you are ready, go through these doors, then turn to the left",
                "and go into the big building."});
        this.box.addBubble(new String[]{"We tried to finish this game,",
                "but we bring you nic menu and MLG trees instead."});
        this.box.addBubble(new String[]{"We are sorry for your disappointment",
                "it will be better next time, I promise!"});
    }

    @Override
    public void tick() {
        super.tick();

        if (this.getPlayer().getBB().intersects(endRect)) {
            this.getPlayer().getBB().setPosition(this.getPlayer().getBB().x1(),
                    this.getPlayer().getBB().y1() + 1.5f);
            this.shouldEnd = 1;
        }
    }

    @Override
    public void setPlayer(Player p) {
        super.setPlayer(p);
        p.disable();
    }

    @Override
    public void onKeyDown(int key) {
        super.onKeyDown(key);

        if (key == IInputManager.Keys.ENTER || key == IInputManager.Keys.SPACE) {
            if (!this.box.next()) {
                this.getPlayer().enable();
                this.box.setVisible(false);
            }
        }
    }

    @Override
    public void renderTick(float ptt) {
        super.renderTick(ptt);
        this.box.renderTick(ptt);
    }
}
