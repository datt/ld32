package cz.dat.ld32.level;

import cz.dat.gaben.api.ContentManager;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.entity.Entity;
import cz.dat.ld32.level.entity.Player;
import cz.dat.ld32.level.entity.Scientist;
import cz.dat.ld32.level.tile.Tile;
import cz.dat.ld32.level.tile.TileMap;
import cz.dat.ld32.level.world.WorldOne;
import cz.dat.ld32.level.world.WorldOut;

import javax.imageio.ImageIO;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class WorldLoader {

    private ContentManager content;
    private TheGame game;

    public WorldLoader(TheGame game) {
        this.game = game;
        this.content = game.getContent();
    }

    public World loadWorld(int worldNumber) throws IOException {
        World ret = null;
        List<Entity> entities = null;

        switch (worldNumber) {
            case 0:
                ret = new WorldOne(this.loadMap("maps/map0_?"), this.game);
                entities = this.loadEntities("maps/map0_?");
                break;
            case 1:
                ret = new WorldOut(this.loadMap("maps/map_out_?"), this.game);
                entities = this.loadEntities("maps/map_out_?");
                break;
        }

        final World finalRet = ret;

        if (finalRet != null) {
            if (entities != null) {
                entities.forEach(e -> {
                    if (e instanceof Player) {
                        finalRet.setPlayer((Player) e);
                    } else {
                        finalRet.addEntity(e);
                    }
                });
            }
            finalRet.getMap().checkBlocks();
        }

        return finalRet;
    }

    public List<Entity> loadEntities(String name) throws IOException {
        List<Entity> entities = new ArrayList<>();
        this.loadEntities(entities, ImageIO.read(this.content.openStream(name.replace("?", "2"))));
        return entities;
    }

    public TileMap loadMap(String name) throws IOException {
        BufferedImage l1 = ImageIO.read(this.content.openStream(name.replace("?", "1")));
        BufferedImage l2 = ImageIO.read(this.content.openStream(name.replace("?", "2")));

        TileMap t = new TileMap(l1.getWidth(), l1.getHeight());
        this.loadLayer(t, 0, l1);
        this.loadLayer(t, 1, l2);

        return t;
    }

    private void loadLayer(TileMap t, int layer, BufferedImage img) {
        for (int x = 0; x < img.getWidth(); x++) {
            for (int y = 0; y < img.getHeight(); y++) {
                int rgb = img.getRGB(x, y);
                Tile tile = Tile.getTile(rgb);

                if (tile != null)
                    t.setTile(x, y, layer, tile);
            }
        }
    }

    private void loadEntities(List<Entity> entities, BufferedImage img) {
        for (int x = 0; x < img.getWidth(); x++) {
            for (int y = 0; y < img.getHeight(); y++) {
                int rgb = img.getRGB(x, y);

                switch (rgb) {
                    case 0xFFabcdef:
                        entities.add(new Player(this.game, x, y, 0.5f, 0.5f));
                        continue;
                    case 0xFFfedcba:
                        entities.add(new Scientist(this.game, x, y, 0.5f, 0.5f));
                        continue;
                }
            }
        }
    }
}
