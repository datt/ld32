package cz.dat.ld32.level;

import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.api.interfaces.ITickListener;
import cz.dat.gaben.util.Vector2;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.entity.Camera;
import cz.dat.ld32.level.entity.Entity;
import cz.dat.ld32.level.entity.Monster;
import cz.dat.ld32.level.entity.Player;
import cz.dat.ld32.level.tile.DoorTile;
import cz.dat.ld32.level.tile.Tile;
import cz.dat.ld32.level.tile.TileMap;

import java.util.*;

public class World implements ITickListener, IInputManager.IInputListener {

    protected Player p;
    protected int shouldEnd = -1;
    Random rnd = new Random();
    private List<Entity> entities, entitiesToAdd;
    private TileMap map;
    private int blockSize = 64;
    private TheGame game;
    private Camera camera;
    private ITexture cursor;
    private Vector2 mousePos;

    public World(TileMap map, TheGame game) {
        this.game = game;
        this.map = map;
        this.entities = new LinkedList<>();
        this.entitiesToAdd = new ArrayList<>();
        this.cursor = game.getApi().getTexture().getTexture(4);
        this.mousePos = game.getApi().getInput().getMousePosition();
        this.addEntity(new Monster(this.game, 2, 2, 1, 1));
    }

    public Camera getCamera() {
        return camera;
    }

    public void addEntity(Entity e) {
        this.entitiesToAdd.add(e);
    }

    public void removeEntity(Entity e) {
        e.remove();
    }

    public int getBlockSize() {
        return this.blockSize;
    }

    public void setBlockSize(int bs) {
        this.blockSize = bs;
    }

    public TileMap getMap() {
        return this.map;
    }

    public void setMap(TileMap map) {
        this.map = map;
    }

    public Player getPlayer() {
        return this.p;
    }

    public void setPlayer(Player p) {
        this.p = p;
        this.camera = new Camera(this.p, this.game.getWidth() / (float) this.getBlockSize(),
                this.game.getHeight() / (float) this.getBlockSize());
        this.addEntity(this.p);
    }

    public int shouldAnotherLevel() {
        return this.shouldEnd;
    }

    public void setNextLevel(int l) {
        this.shouldEnd = l;
    }

    @Override
    public void tick() {   	
    	for (Iterator<Entity> iterator = this.entities.iterator(); iterator.hasNext(); ) {
            Entity e = iterator.next();
            e.tick();

            if (e.shouldBeRemoved()) {
            	iterator.remove();
            }
        }

        for (Iterator<Entity> iterator = this.entitiesToAdd.iterator(); iterator.hasNext(); ) {
            Entity e = iterator.next();
            e.setWorld(this);
            this.entities.add(e);
            iterator.remove();
        }

        int mX = (int) this.getPlayer().getBB().x1();
        int mY = (int) this.getPlayer().getBB().y1();

        for (int x = (mX - 1); x < (mX + 2); x++) {
            for (int y = (mY - 1); y < (mY + 2); y++) {
                if (this.getMap().getTileAt(x, y, 1).is(DoorTile.door)) {
                    int[] state = (int[]) this.getMap().getState(x, y, 1);
                    boolean o = this.game.getApi().getInput().isKeyDown(IInputManager.Keys.SPACE);

                    state[2] = o ? 1 : 0;
                    ((int[]) this.getMap().getState(state[0], state[1], 1))[2] = state[2];

                    this.getMap().checkBlock(x, y, 1);
                    this.getMap().checkBlock(state[0], state[1], 1);
                    break;
                }
            }
        }
    }

    @Override
    public void renderTick(float ptt) {
        for (int x = 0; x < this.map.getWidth(); x++) {
            for (int y = 0; y < this.map.getHeight(); y++) {
                ITexture tex = this.map.getTileAt(x, y, 0).getTexture();

                if (tex != null)
                    this.game.getApi().getRenderer().drawTexture(tex, x * this.blockSize, y * this.blockSize,
                            x * this.blockSize + this.blockSize, y * this.blockSize + this.blockSize);

                tex = this.map.getTileAt(x, y, 1).getTexture();

                if (tex != null) {
                    if (tex != Tile.tree.getTexture()) {
                        this.game.getApi().getRenderer().drawTexture(tex, x * this.blockSize, y * this.blockSize,
                                x * this.blockSize + this.blockSize, y * this.blockSize + this.blockSize);
                    }
                }
            }
        }

        for (Iterator<Entity> iterator = this.entities.iterator(); iterator.hasNext(); ) {
            Entity e = iterator.next();
            e.renderTick(ptt);
        }

        for (int x = 0; x < this.map.getWidth(); x++) {
            for (int y = 0; y < this.map.getHeight(); y++) {
                ITexture tex = this.map.getTileAt(x, y, 1).getTexture();
                if (tex == Tile.tree.getTexture()) {
                    this.game.getApi().getRenderer().drawTextureRotated(tex, x * this.blockSize + blockSize / 2,
                            y * this.blockSize + blockSize / 2, rnd.nextInt(360), 6);
                }
            }
        }

        this.game.getApi().getRenderer().translate(0, 0, false);
        this.game.getApi().getRenderer().drawTexture(this.cursor,
                this.game.getApi().getInput().getMousePosition().x() - 16,
                this.game.getApi().getInput().getMousePosition().y() - 16);
    }

    @Override
    public void onKeyDown(int key) {

    }

    @Override
    public void onKeyUp(int key) {

    }

    @Override
    public void onChar(char typedChar) {

    }

    @Override
    public void onMousePositionChange(float x, float y) {

    }

    @Override
    public void onMouseButtonDown(int button) {
        if (button == IInputManager.Keys.MOUSE_LEFT) {
        	this.p.shoot(0);
        } else if (button == IInputManager.Keys.MOUSE_RIGHT) {
            this.p.shoot(1);
        }
    }

    @Override
    public void onMouseButtonUp(int button) {

    }

    @Override
    public void onMouseScroll(float x, float y) {

    }
}
