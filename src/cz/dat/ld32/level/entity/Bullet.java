package cz.dat.ld32.level.entity;

import java.util.Random;

import cz.dat.gaben.util.AABB;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.MapAABB;

public class Bullet extends Entity {
	
	float rot = 0;
	
	public Bullet(TheGame game, float x, float y, float width, float height, float speedX, float speedY) {
		super(game, x, y, width, height);
		this.rot = (float) Math.random()*360;
		this.velX = speedX;
		this.velY = speedY;
	}

	@Override
	public void enable() {

	}

	@Override
	public void disable() {

	}

	@Override
	public void tick() {
		this.lastBB = new MapAABB(this.bb);
		
		this.move();
	}

	@Override
	public void renderTick(float ptt) {
		this.renderBB = this.lastBB.mix(this.bb, ptt);

        float halfw = this.renderBB.width() / 2;
        float halfh = this.renderBB.height() / 2;
        
        this.game.getApi().getRenderer().drawTextureRotated(this.game.getApi().getTexture().getTexture(6), (this.renderBB.x1()+halfw)*this.world.getBlockSize(), (this.renderBB.y1()+halfh)*this.world.getBlockSize(), this.rot, 1);
        
	}
	
    @Override
	public void move() {
    	float[] clip = this.bb.moveCollide(this.world.getMap(), this.velX, this.velY);

        if(this.velY != clip[1]) {
            this.remove();
        }

        if(this.velX != clip[0]) {
            this.remove();
        }
         
        
    }

}
