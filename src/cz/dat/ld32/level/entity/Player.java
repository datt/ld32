package cz.dat.ld32.level.entity;

import cz.dat.gaben.api.interfaces.IInputManager;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.AABB;
import cz.dat.gaben.util.Vector2;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.MapAABB;

public class Player extends Entity {

    private int currentImage = 0;
    private float timer = 0;
    private ITexture[] textures;
    private int nis = 1, nie = 2;//, bis = 3, bie = 5;

    private float speed = 0.1f;
    private float rot = 0;
    private Vector2 mousePos;
    private boolean enabled = true;

    public Player(TheGame game, float x, float y, float width, float height) {
        super(game, x, y, width, height);
        this.mousePos = this.game.getApi().getInput().getMousePosition();

        this.textures = new ITexture[game.getApi().getTexture().getSpritesheet(3).size()];
        game.getApi().getTexture().getSpritesheet(3).forEach((i, t) -> this.textures[i] = t);
    }

    @Override
    public void enable() {
        this.enabled = true;
    }

    @Override
    public void disable() {
        this.enabled = false;
    }

    public void shoot(int type) {
        float x = (float) -Math.sin(Math.toRadians(this.rot));
        float y = (float) Math.cos(Math.toRadians(this.rot));
    	
    	this.world.addEntity(new Bullet(this.game, this.bb.x1(), this.bb.y1(), 0.5f, 0.5f, x*0.5f, y*0.5f));
    }
    
    @Override
    public void tick() {
        this.lastBB = new MapAABB(this.bb);

        if (this.enabled) {
            if (this.game.getApi().getInput().isKeyDown(IInputManager.Keys.W)) {
                this.velY -= this.speed;
            }

            if (this.game.getApi().getInput().isKeyDown(IInputManager.Keys.S)) {
                this.velY += this.speed;
            }

            if (this.game.getApi().getInput().isKeyDown(IInputManager.Keys.A)) {
                this.velX -= this.speed;
            }

            if (this.game.getApi().getInput().isKeyDown(IInputManager.Keys.D)) {
                this.velX += this.speed;
            }

            this.velX *= 0.65f;
            this.velY *= 0.65f;

            if (Math.abs(this.velX) > 0.01 || Math.abs(this.velY) > 0.01) {
                this.timer += Math.min(0.167, Math.abs(this.velX) + Math.abs(this.velY))*2;
            } else {
                this.currentImage = 0;
            }

            this.move();
        }

        float x = (float) -Math.sin(Math.toRadians(this.rot));
        float y = (float) Math.cos(Math.toRadians(this.rot));

        this.world.getCamera().target((this.bb.x1() + this.bb.width() / 2) - this.velX * 2 + x, (this.bb.y1() + this.bb.height() / 2) - this.velY * 2 + y);
        this.world.getCamera().tick();

        if (this.timer > 2) {
            this.currentImage++;
            if (this.currentImage > this.nie)
                this.currentImage = this.nis;
            this.timer = 0;
        }
    }

    @Override
    public void renderTick(float ptt) {
        this.renderBB = this.lastBB.mix(this.bb, ptt);

        float halfw = this.renderBB.width() / 2;
        float halfh = this.renderBB.height() / 2;

        float ox = this.world.getCamera().getXO(ptt) * this.world.getBlockSize();
        float oy = this.world.getCamera().getYO(ptt) * this.world.getBlockSize();

        this.rot = (float) Math.toDegrees(Math.atan2((oy + this.mousePos.y() - ((this.renderBB.y1() + halfh) * this.world.getBlockSize())),
                (ox + this.mousePos.x() - ((this.renderBB.x1() + halfw) * this.world.getBlockSize())))) - 90;

        this.game.getApi().getRenderer().drawTextureRotated(this.textures[this.currentImage],
                (this.renderBB.x1() + halfw) * this.world.getBlockSize(),
                (this.renderBB.y1() + halfh) * this.world.getBlockSize(),
                this.rot, 3);
    }
}
