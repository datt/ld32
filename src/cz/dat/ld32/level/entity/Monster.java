package cz.dat.ld32.level.entity;

import cz.dat.gaben.util.Vector2;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.MapAABB;

public class Monster extends Entity {

	float rot = 0;
	
	public Monster(TheGame game, float x, float y, float width, float height) {
		super(game, x, y, width, height);
	}
	
	@Override
	public void enable() {
		
	}

	@Override
	public void disable() {
		
	}

	@Override
	public void tick() {
		this.lastBB = new MapAABB(this.bb);
		
		this.velX = (float) -Math.sin(Math.toRadians(this.rot))*0.1f;
        this.velY = (float) Math.cos(Math.toRadians(this.rot))*0.1f;
        
        this.move();
	}

	@Override
	public void renderTick(float ptt) {
		this.renderBB = this.lastBB.mix(this.bb, ptt);
		
		Vector2 pp = this.world.getPlayer().renderBB.getPosition();
    	
    	float halfw = this.renderBB.width() / 2;
        float halfh = this.renderBB.height() / 2;
        
        float pX = pp.x() + halfw;
        float pY = pp.y() + halfh;

        float a = this.bb.y1() + halfh - pY;
        float b = this.bb.x1() + halfw - pX;
        
        this.rot = (float) Math.toDegrees(Math.atan2(a , b)) + 90;
        
        this.game.getApi().getRenderer().drawTextureRotated(this.game.getApi().getTexture().getSpritesheet(4).get(1), (this.renderBB.x1()+halfw)*this.world.getBlockSize(), (this.renderBB.y1()+halfh)*this.world.getBlockSize(), this.rot, 2);
	}

}
