package cz.dat.ld32.level.entity;

import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.Vector2;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.World;

public class Scientist extends Entity {
    private float rot = 0;
    private Vector2 playerPos;
    private ITexture texture;

    public Scientist(TheGame game, float x, float y, float width, float height) {
        super(game, x, y, width, height);
        this.texture = game.getApi().getTexture().getTexture(3, 6);
    }

    @Override
    public void setWorld(World w) {
        super.setWorld(w);
    }

    @Override
    public void enable() {

    }

    @Override
    public void disable() {

    }

    @Override
    public void tick() {

    }

    @Override
    public void renderTick(float ptt) {
        this.playerPos = this.world.getPlayer().renderBB.getPosition();
    	
    	float halfw = this.bb.width() / 2;
        float halfh = this.bb.height() / 2;
        
        float pX = this.playerPos.x() + halfw;
        float pY = this.playerPos.y() + halfh;

        float a = this.bb.y1() + halfh - pY;
        float b = this.bb.x1() + halfw - pX;
        
        this.rot = (float) Math.toDegrees(Math.atan2(a , b)) + 90;

        this.game.getApi().getRenderer().drawTextureRotated(this.texture,
                (this.bb.x1() + halfw) * this.world.getBlockSize(),
                (this.bb.y1() + halfh) * this.world.getBlockSize(),
                this.rot, 3);
        
    }
}
