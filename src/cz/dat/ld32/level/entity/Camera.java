package cz.dat.ld32.level.entity;

public class Camera {

    private float posX;
    private float posY;

    private float lastPosX;
    private float lastPosY;

    private float targetX;
    private float targetY;

    private float w;
    private float h;

    public Camera(Player p, float w, float h) {
        this.w = w;
        this.h = h;
        this.lastPosX = this.posX = p.bb.x1() + p.bb.width() / 2;
        this.lastPosY = this.posY = p.bb.y1() + p.bb.height() / 2;
    }

    public void target(float x, float y) {
        this.targetX = x;
        this.targetY = y;
    }

    public void tick() {
        this.lastPosX = posX;
        this.lastPosY = posY;

        this.posX = posX + (targetX - posX) / 4f;
        this.posY = posY + (targetY - posY) / 4f;
    }

    public float getX(float ptt) {
        return this.lastPosX + (posX - lastPosX) * ptt;
    }

    public float getY(float ptt) {
        return this.lastPosY + (posY - lastPosY) * ptt;
    }

    public float getXO(float ptt) {
        return (this.lastPosX + (posX - lastPosX) * ptt) - w / 2f;
    }

    public float getYO(float ptt) {
        return (this.lastPosY + (posY - lastPosY) * ptt) - h / 2f;
    }

}
