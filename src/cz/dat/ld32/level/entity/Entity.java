package cz.dat.ld32.level.entity;

import cz.dat.gaben.api.interfaces.IAnimation;
import cz.dat.gaben.util.AABB;
import cz.dat.ld32.TheGame;
import cz.dat.ld32.level.MapAABB;
import cz.dat.ld32.level.World;

public abstract class Entity implements IAnimation {

    protected TheGame game;
    protected World world;
    protected MapAABB bb;

    protected MapAABB lastBB;
    protected AABB renderBB;
    
    protected float velX, velY;
    
    protected boolean remove = false;

    public Entity(TheGame game, float x, float y, float width, float height) {
        this.game = game;
        this.bb = new MapAABB(x, y, width, height);
        this.renderBB = new AABB(this.bb);
        this.lastBB = new MapAABB(this.bb);
    }

    public void setWorld(World w) {
        this.world = w;
    }

    public void remove() {
    	this.remove = true;
    }
    
    public boolean shouldBeRemoved() {
    	return this.remove;
    }

    public MapAABB getBB() {
        return this.bb;
    }

    public void move() {
        float[] clip = this.bb.moveCollide(this.world.getMap(), this.velX, this.velY);

        if(this.velY != clip[1]) {
            this.velY = 0;
        }

        if(this.velX != clip[0]) {
            this.velX = 0;
        }
         
        
    }
}