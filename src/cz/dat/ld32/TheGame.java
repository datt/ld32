package cz.dat.ld32;

import cz.dat.gaben.api.ContentManager;
import cz.dat.gaben.api.Settings;
import cz.dat.gaben.api.Settings.Wrap;
import cz.dat.gaben.api.data.IDataProvider;
import cz.dat.gaben.api.data.TextDataProvider;
import cz.dat.gaben.api.exception.DataSyncException;
import cz.dat.gaben.api.game.Game;
import cz.dat.gaben.api.game.GameWindowFactory;
import cz.dat.gaben.api.interfaces.IRenderer;
import cz.dat.gaben.api.interfaces.ITexture;
import cz.dat.gaben.util.Color;
import cz.dat.gaben.util.Logger;
import cz.dat.gaben.util.Vector2;
import cz.dat.ld32.screen.AboutScreen;
import cz.dat.ld32.screen.GameScreen;
import cz.dat.ld32.screen.MenuScreen;
import cz.dat.ld32.screen.OptionsScreen;

import javax.imageio.ImageIO;
import java.io.File;
import java.io.IOException;

public class TheGame extends Game{

    public static final Color SEXY_BLUE = new Color(0, 110, 185);
    public static final Color DARKER_GRAY = Color.fromArgb(0xFF191919);
    private IDataProvider data;
    private ITexture cloudsTexture;

    public TheGame(IDataProvider data) {
        this.data = data;
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    public static void main(String[] args) throws IOException, DataSyncException {
        ContentManager manager = new ContentManager("/cz/dat/ld32/res", true);
        GameWindowFactory.enableSplashscreen(true, ImageIO.read(manager.openStream("textures/logo")));

        File f = new File("config.cfg");
        if (!f.exists())
            f.createNewFile();

        TextDataProvider p = new TextDataProvider(f.getAbsolutePath());
        p.sync(IDataProvider.MergeType.KEEP_LOCAL);

        Integer width = p.getData("width", Integer.class);
        Integer height = p.getData("height", Integer.class);

        if (width == null)
            p.setData("width", 1280, Integer.class);
        if (height == null)
            p.setData("height", 720, Integer.class);
        if (p.getDataType("sound") == null)
            p.setData("sound", 100, Integer.class);
        if (p.getDataType("blood") == null)
            p.setData("blood", false, Boolean.class);

        Thread t = new Thread(GameWindowFactory.createPreferredGame(width == null ? 1280 : width,
                height == null ? 720 : height, manager, new TheGame(p)));
        t.run();
    }

    @Override
    public void init() {
        super.init();

        this.getWindow().setTitle("The Secret of MD");

        this.getApi().getSettings().setMinFilter(Settings.Filters.MIPMAP_LINEAR);
        this.getApi().getSettings().setMagFilter(Settings.Filters.NEAREST);

        Settings mirrorSettings = new Settings();
        mirrorSettings.setWrapS(Wrap.MIRRORED_REPEAT);
        mirrorSettings.setWrapT(Wrap.MIRRORED_REPEAT);
        mirrorSettings.setMinFilter(Settings.Filters.MIPMAP_LINEAR);
        mirrorSettings.setMagFilter(Settings.Filters.NEAREST);

        this.getApi().getTexture().loadTexture(0, "textures/logo");
        this.getApi().getTexture().loadTexture(1, "textures/arrow");
        this.getApi().getTexture().loadTexture(2, "textures/arrowb");
        this.getApi().getTexture().loadTexture(3, "textures/clouds");
        this.getApi().getTexture().loadTexture(4, "textures/mlg");
        this.getApi().getTexture().loadTexture(5, "textures/enter", mirrorSettings);
        this.getApi().getTexture().loadTexture(6, "textures/splash", mirrorSettings);

        this.getApi().getTexture().loadSpritesheet(0, "textures/spritesheet", new Vector2(16, 16), mirrorSettings);
        this.getApi().getTexture().loadSpritesheet(1, "textures/wall", new Vector2(16, 16), mirrorSettings);
        this.getApi().getTexture().loadSpritesheet(2, "textures/door", new Vector2(16, 16), mirrorSettings);
        this.getApi().getTexture().loadSpritesheet(3, "textures/char", new Vector2(24, 16), mirrorSettings);
        this.getApi().getTexture().loadSpritesheet(4, "textures/monster", new Vector2(16, 16), mirrorSettings);

        this.getApi().getFont().loadFont("basic", "fonts/mammagamma", 128);
        this.getApi().getTexture().finishLoading();

        this.cloudsTexture = this.getApi().getTexture().getTexture(3);

        this.getApi().getSound().addSound(0, "sounds/menu");
        this.getApi().getSound().addSound(1, "sounds/menugo");

        Integer sound = this.data.getData("sound", Integer.class);
        this.getApi().getSound().setVolume((sound != null) && (sound != 0), sound == null ? 0 : sound / 100f);

        this.getApi().getRenderer().clearColor(TheGame.DARKER_GRAY);

        // 0 - Main menu
        // 1 - Game
        // 2 - Options
        // 3 - About
        this.addScreen(0, new MenuScreen(this));
        this.addScreen(1, new GameScreen(this));
        this.addScreen(2, new OptionsScreen(this));
        this.addScreen(3, new AboutScreen(this));
        this.openScreen(0);
    }

    public void drawClouds() {
        float so = (float) (System.nanoTime() / 10000000000D);
        float som = so * 0.5f;
        float son = so * 0.42f;
        int size = this.getWidth();
        IRenderer r = this.getApi().getRenderer();

        if (this.data.getData("blood", Boolean.class))
            r.color(1f, 0, 0, 0.67f);
        else
            r.color(0.4f, 0.5f, 0.7f, 0.2f);

        this.getApi().getRenderer().texture(this.cloudsTexture);

        r.texCoord(0 + so, 0 + so);
        r.vertex(0, 0);
        r.texCoord(1 + so, 0 + so);
        r.vertex(size, 0);
        r.texCoord(1 + so, 1 + so);
        r.vertex(size, size);

        r.texCoord(0 + so, 0 + so);
        r.vertex(0, 0);
        r.texCoord(1 + so, 1 + so);
        r.vertex(size, size);
        r.texCoord(so, 1 + so);
        r.vertex(0, size);

        r.texCoord(0 + som, 0 + son);
        r.vertex(0, 0);
        r.texCoord(1 + som, 0 + son);
        r.vertex(size, 0);
        r.texCoord(1 + som, 1 + son);
        r.vertex(size, size);

        r.texCoord(0 + som, 0 + son);
        r.vertex(0, 0);
        r.texCoord(1 + som, 1 + son);
        r.vertex(size, size);
        r.texCoord(som, 1 + son);
        r.vertex(0, size);

        r.flush();
    }

    @Override
    public void cleanup() {
        super.cleanup();
        try {
            this.data.sync(IDataProvider.MergeType.PREFER_MEMORY);
        } catch (DataSyncException e) {
            Logger.get().warnException("when saving config", e);
        }
    }

    public IDataProvider getDataProvider() {
        return this.data;
    }
}
